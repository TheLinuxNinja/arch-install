#!/usr/bin/env bash
# This will not run in the chroot environment - maybe this could be handled another way
#/usr/bin/plasma-apply-lookandfeel -a org.kde.breezetwilight.desktop
[ ! -d /etc/sddm.conf.d ] && sudo mkdir -p /etc/sddm.conf.d
cat << EOF > /tmp/kde_settings.conf
[Theme]
Current=breeze
EOF
sudo chown root:root /tmp/kde_settings.conf
sudo mv /tmp/kde_settings.conf /etc/sddm.conf.d/
kwriteconfig5 --file baloofilerc --group 'Basic Settings' --key 'Indexing-Enabled' 'false'
kwriteconfig5 --file baloofilerc --group 'General' --key 'only basic indexing' 'true'
kwriteconfig5 --file kactivitymanagerd-pluginsrc --group 'Plugin-org.kde.ActivityManager.Resources.Scoring' --key 'keep-history-for' '1'
kwriteconfig5 --file kactivitymanagerd-pluginsrc --group 'Plugin-org.kde.ActivityManager.Resources.Scoring' --key 'what-to-remember' '2'
kwriteconfig5 --file kactivitymanagerdrc --group 'Plugins' --key 'org.kde.ActivityManager.ResourceScoringEnabled' 'false'
kwriteconfig5 --file kcminputrc --group 'Mouse' --key 'XLbInptNaturalScroll' 'true'
kwriteconfig5 --file kcminputrc --group 'Keyboard' --key 'KeyRepeat' 'repeat'
kwriteconfig5 --file kcminputrc --group 'Keyboard' --key 'NumLock' '0'
kwriteconfig5 --file kcminputrc --group 'Keyboard' --key 'RepeatDelay' '600'
kwriteconfig5 --file kcminputrc --group 'Keyboard' --key 'RepeatRate' '25'
kwriteconfig5 --file kdeglobals --group 'KDE' --key 'SingleClick' 'false'
kwriteconfig5 --file kglobalshortcutsrc --group 'ksmserver' --key '_k_friendly_name' 'Session Management'
kwriteconfig5 --file kglobalshortcutsrc --group 'plasmashell' --key '_k_friendly_name' 'Activity switching'
kwriteconfig5 --file kmixrc --group 'Global' --key 'AutoStart' 'false'
kwriteconfig5 --file krunnerrc --group 'Plugins' --key 'Spell CheckerEnabled' 'true'
kwriteconfig5 --file ksmserverrc --group 'General' --key 'confirmLogout' 'false'
kwriteconfig5 --file ksmserverrc --group 'General' --key 'loginMode' 'emptySession'
kwriteconfig5 --file kwinrc --group 'Compositing' --key 'GLCore' 'true'
kwriteconfig5 --file kwinrc --group 'org.kde.kdecorations2' --key 'BorderSize' 'Tiny'
kwriteconfig5 --file kwinrc --group 'org.kde.kdecorations2' --key 'BorderSizeAuto' 'false'
kwriteconfig5 --file kwinrc --group 'org.kde.kdecorations2' --key 'ButtonsOnLeft' 'M'
kwriteconfig5 --file kwinrc --group 'org.kde.kdecorations2' --key 'ButtonsOnRight' 'IAX'
kwriteconfig5 --file kwinrc --group 'org.kde.kdecorations2' --key 'CloneOnDoubleClickOnMenu' 'true'
kwriteconfig5 --file kwinrc --group 'org.kde.kdecorations2' --key 'ShowToolTips' 'false'
kwriteconfig5 --file kxkbrc --group 'Layout' --key 'DisplayNames' ''
kwriteconfig5 --file kxkbrc --group 'Layout' --key 'LayoutList' 'US'
#kwriteconfig5 --file kxkbrc --group 'Layout' --key 'LayoutLoopCount' '-1'
kwriteconfig5 --file kxkbrc --group 'Layout' --key 'Model' 'pc105'
kwriteconfig5 --file kxkbrc --group 'Layout' --key 'ResetOldOptions' 'false'
kwriteconfig5 --file kxkbrc --group 'Layout' --key 'SwitchMode' 'Global'
kwriteconfig5 --file kxkbrc --group 'Layout' --key 'Use' 'true'
kwriteconfig5 --file kxkbrc --group 'Layout' --key 'VariantList' ''
kwriteconfig5 --file kwinrc --group 'Plugins' --key 'kwin4_effect_dimscreenEnabled' 'true'
kwriteconfig5 --file kwinrc --group 'Windows' --key 'FocusStealingPreventionLevel' '0'
kwriteconfig5 --file systemsettingsrc --group 'Notification Messages' --key 'welcome' 'false'
