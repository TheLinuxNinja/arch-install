#### WARNING!!

This arch-install.sh script, when run, will ERASE YOUR HARD DRIVE(S)!!
Do NOT run this script if you do not know what you are doing!!!

When using a virtual machine, this isn't as much of a concern, but I added this
warning just to prevent you from trying to run this on your bare-metal system
and being upset that all your data is gone.

This script is intended to allow you to quickly get a minimal Arch Linux Machine
up and running. It is designed to install Arch Linux, and optionally install the
KDE Plasma, XFCE, Gnome, MATE, or LXQt Desktop Environment.

At the end of the installation, if you leave the customizegui variable set to
'true', there are some custom Desktop Environment settings applied. These are a
heavily opinionated list of either KDE Plasma, XFCE, Gnome, MATE, or LXQt DE
settings that should be run as the normal user. As I find the settings I want to
be changed, I will add them to the appropriate script. The goal is to be able to
bring up a new Arch Linux Machine with the KDE Plasma, XFCE, Gnome, MATE, or
LXQt DE and not have to spend too much time making popular customizations. If
there's a customization you think I missed, please open an issue.

The arch-install.sh script is designed to be run from the Arch ISO. It will make
a simple check to see if you are in fact running it from within the Arch ISO
environment.

This script has been tested to work on Arch ISO version 2022.10.01. Using some
previous versions is known to cause issues due to changes made in the Arch
Repository. If you find something that causes errors with this script, please
open an Issue. Also please keep in mind that Arch Linux is ever evolving.
Package names change. Sometimes, such a change will break scripts such as these,
especially if a meta package is changed. The changes to the base package group
(making it a meta-package and removing all non-essential packages) is such a
change. These types of changes will make it necessary to continuously update
this script and others like it.

The top of this script contains user-configurable variables. This script
requires you to set these variables. At a minimum, you MUST change the default
user's password and the root password. If you run this script without setting
the variables to your needs, it will exit with an error. These variables allow
you to change the parameters of the script in some ways. The variables are
documented within the script, not in this README.

Left at the default settings:
- It assumes en_US.UTF-8 language and United States keyboard.
- The time zone is automatically determined based on your Public IP address.
- The country to select when determining the best Arch Repo mirror is
  determined based on your Public IP address. If no country is found, or your
  country does not have Arch Repositories listed, repositories are selected
  without specifying a specific country.
- It will partition all disk(s) with the GPT (GUID Partition Table) using 1MB
  for the BIOS Boot Partition, 512MB for the EFI System Partition (if booting
  an EFI System), 1GB for /boot, 4GB for swap, and the remaining space for your
  '/' filesystem.
- It uses the Logical Volume Manager for swap, '/boot', and '/' and allocates
  80% of the Volume Group.
- Filesystems are formatted using ext4.
- It installs openssh
- It installs NetworkManager
- It installs man pages
- It installs the correct ucode package for your CPU
- It installs vi and vim editors
- It installs all the packages necessary to run KDE Plasma, XFCE, Gnome, MATE,
  or LXQt if you set the 'installgui' variable to either 'kde', 'xfce', 'gnome',
  'mate', or 'lxqt'.
- It installs all xorg-drivers if you install the GUI.
- It detects whether you're running a virtual machine under VirtualBox, VMware,
  or QEMU/KVM. It installs either the VirtualBox Guest Utils, open-vm-tools, or
  QEMU Guest Agent as appropriate.
- It enables the [multilib] repository.
- It installs the linux kernel.
- It installs the linux-firmware package if you're not in a virtualized
  environment.
- It installs nftables in place of iptables.

Some of the above defaults are easy to change using the variables at the top of
this script. The settings that aren't set with a variable can be changed by
editing the script yourself. Please open an Issue if there is something you
recommend I do to make installing a Minimal Arch Linux system better.

The newly created user is placed in the 'wheel' group which gives it 'sudo'
rights. The 'wheel' group is configured in sudoers to not require a password
when using 'sudo'. This is required for automated package installation using
'yay' which must be run as a normal user with 'sudo' rights. If this is not
desired behavior, please use `visudo` to make the appropriate changes. A file
with the same name as the user is also placed in the /etc/sudoers.d/ directory
giving the user sudo rights without requiring a password. This makes your user
able to always run sudo commands without a password, even if you decide to
remove that right from the 'wheel' group using 'visudo'.

At last calculation, this script will by default download and install over
1,000 packages totaling approximately 1.8GB of downloaded files from your
selected Arch Linux Repository Mirror or the automatically chosen mirror if you
leave the script to figure out what mirror to use. (Using defaults, there is no
GUI installed.)

If using a Virtual Machine, the resulting virtual disk file will be
approximately 8.7GB in size depending on changes to the Arch repo packages and
your selected swap size. Add your swap size to this in order to calculate your
minimum Virtual Disk size. Currently, a disk less than 15GB may cause disk
space issues when using a 4GB Swap size.

On a fast system, with a fast disk, a fast network, and a fast local Arch
Repository Mirror, this script can take as little as 1 minute to completely
install an Arch Linux Machine with a KDE Plasma GUI. Your mileage may vary
significantly. Don't be surprised if your machine takes 30 minutes or longer to
install due to the bandwidth available between you and the Arch Repository
Mirror. The speed of your disk can also cause the install to take longer.

If the script should fail for some reason, running it again will erase the
disk(s) and start over. Nothing from a previous run is saved for a subsequent
run, except that config files within the Arch ISO will maintain the changes
made to them, such as /etc/pacman.conf and /etc/pacman.d/mirrorlist (which is
backed up to /etc/pacman.d/mirrorlist.backup and overwritten on each subsequent
run).

When this script has finished, safely power off the machine, eject the Arch ISO
from the CDROM drive, and boot the machine into Arch Linux!

## Usage:

Download the [Arch Linux Installation ISO](https://www.archlinux.org/download/)

# VirtualBox Instructions:

You should create a new VirtualBox Virtual Machine as type "Arch Linux 64-bit".
I have tested this script with as little as 2GB RAM. 1GB of RAM will cause GPG
to fail to verify package signatures.

The default virtual disk size of 8GB is not big enough. I recommend starting
with 20GB or more. You can create multiple disks. Each will be added to the LVM
Volume Group, if you choose to enable LVM. In Settings -> System, check the
'Enable EFI' box (to use UEFI), and set the Display Graphics Controller to
'VBoxSVGA'.

Assign the ISO to the CDROM device, and boot the VM.

# VMware Instructions (Workstation 15.x Pro):

- You should create a new VMware Virtual Machine
- Select "Custom (advanced)"
  I used Workstation 15.x Compatibility, but this shouldn't matter.
- Install operating system from: "Use ISO image:" and browse for the Arch ISO
  you downloaded.
- Guest Operating System: "Linux" Version: "Other Linux 5.x or later kernel
  64-bit"
- Virtual Machine Name: Change to something more descriptive than the default
- Accept the default Location: unless you want to put the Virtual Machines files
  elsewhere
- Consider increasing the "Total processor cores:" count
- Increase the "Memory for this virtual machine:" to at least 2048MB
- Network Connection is up to you depending on your configuration
- SCSI Controller: I tested with LSI Logic, although others may work
- Virtual Disk Type: I tested SCSI, SATA, and NVMe
- Disk: Recommended to "Create a new virtual disk" of at least 15GB

Once you have the Virtual Machine created, do not boot it.
You need to "Edit Virtual Machine Settings"
- Select the "Options" tab
- Select "Advanced"
- Change the "Firmware type" from "BIOS" to "UEFI" (to use UEFI Boot)

# QEMU/KVM

- The video for an Arch VM needs to be set to use VirtIO instead of QXL
- The QEMU Guest Agent will be installed and enabled automatically

# All platforms:

Once you have the Arch ISO booted, select the first menu option which will take
you to a root shell prompt, or just let the timer countdown on its own and wait
for the root shell prompt.

(Do NOT perform any other disk-related actions, like manually adding/removing
partitions, or doing anything with LVM configurations. If you break LVM before
running the script, the script will not work, as it assumes LVM is in a good
state. If you must do any of these things, reboot into the Arch ISO again after
making your changes before running the script.)

Download the arch-install.sh program from GitLab and make it executable:

[arch-install.sh](https://thelinuxninja.gitlab.io/arch-install)
Edit the variables at the top to your preferences.

~~~sh
curl -L -o arch-install.sh https://thelinuxninja.gitlab.io/arch-install
vim arch-install.sh
chmod u+x arch-install.sh
./arch-install.sh
~~~
