# BTRFS feature introduction

While BTRFS has been around for a while, it has only recently become a more 'stable' filesystem worth deploying to personal Linux workstations. BTRFS is still marked as "Not Production Ready" and you are required to keep good backups for when problems do arise. The addition of support for BTRFS to this script is new, so here is my thought process:

- If I'm going to use BTRFS, I must ask myself if there is any good reason to place BTRFS on top of LVM. Currently, I can see no benefit. Therefore, when selecting to use the BTRFS filesystem, LVM will not be implemented during the install.
- At the moment, /boot is not using BTRFS, but is still setup to use ext4. I'm going to change this in the next iteration of this script, since being able to roll back the kernel/initram is a huge benefit of using BTRFS. However, rolling back /boot without rolling back the subvolume mounted a '/' would cause a problem since the kernel modules are versioned and much be kept together with the 
- If I make /boot a BTRFS subvolume, that will introduce a bit of an issue when I implement LUKS. Since Grub cannot unlock a LUKSv2 container, /boot will need to sit on a LUKSv1 container. So, it has been my preference to have two LUKS containers, one just for /boot using LUKSv1, another for the rest of the disk using LUKSv2. By having to split /boot off into a separate LUKS container, that means it won't be a subvolume of the same BTRFS filesystem that the rest of the filesystem will be using, but it will be mounted under '/' as '/boot', mixing two BTRFS backing block devices in the file tree. I hope this doesn't cause an issue with BTRFS tools like Grub-BTRFS when I get there. Once Grub has good support for LUKSv2, this issue goes away and keeping '/boot' on the same BTRFS volume won't be a problem.
- Services like snapper and bees need to be configured. We need a subvolume mounted at /.snapshots - and that will be added, soon.
- I noticed that when installing Arch on a BTRFS filesystem, there are two subvolumes that get automatically created underneath subvolume '@' that is mounted as '/':
  /var/lib/portables
  /var/lib/machines
