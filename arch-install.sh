#!/bin/bash
arch_install_version="1.6.5"

# If you are about to run this script without first reading the README, you're
# doing it wrong.

# This arch-install.sh script, when run, will ERASE YOUR HARD DRIVE(S)!!
# Do NOT run this script if you do not know what you are doing!!!

# If DRYRUN is set to 'true', the functions will all be called but will not
# perform any disk modifications.
: ${DRYRUN='false'}
# The above line will not override the DRYRUN env var if it is already set

# Set debugging if extra output is needed for troubleshooting
# Set the DEBUG env var or edit this file and change the following line to set
# DEBUG='true'
: ${DEBUG:='false'}
# The above line will not override the DEBUG env var if it is already set

# The following 5 lines will set some options only when DEBUG is set
[ ${DEBUG} = 'true' ] && set +x
[ ${DEBUG} = 'true' ] && exec 5> >(logger -t $0)
[ ${DEBUG} = 'true' ] && BASH_XTRACEFD="5"
[ ${DEBUG} = 'true' ] && PS4='$LINENO: '

# Please be sure you have a disk block storage device large enough for the
# install you're requesting. It is recommended to have at LEAST 15GB disk space
# for a full install, but then there will be very little free space after
# install. Set the following environment variables according to your preferences
# Each variable gives you some control over the automated installation.

# The location of the gitlab repo where this file can find updates and other
# associated scripts
archinstallrepo='https://thelinuxninja.gitlab.io/arch-install'

## OS Settings
# Arch Linux Machine Hostname to set
archhostname='arch-host'

# Locale
archlocale='en_US.UTF-8'

# Keymap
archkeymap='us'

# Root password to set
rootpw='rootpasswordgoeshere'

# Normal User name to create
username='user'

# If you're installing a server, you might not want to set a password. Change
# this variable to 'false' to skip setting a password.
setuserpw='true'

# Normal User password to set
userpw='userpasswordgoeshere'

# The contents of this variable will be put in the authorized_keys file for the
# user. Leave it empty if you don't want to manually add a ssh key.
usersshkey=''

# If you have a ssh key located at a URL, enter the URL here. It will be
# downloaded and verified, then added to the authorized_keys file along with the
# key defined by the 'usersshkey' variable above.
usersshkeylocation=''

# Block Device Settings
# If set to 'true', get a list of disk devices and use them all
# (Adds all block devices to LVM Volume Group if LVM is enabled)
autodisklist='true'

# Disk device name to use for installation - CHANGE this to match the disk block
# device name(s) if 'autodisklist' is not set to 'true' or if you only want this
# script to erase/use a specific device!
diskdevs=('/dev/sda')

# EFI System Partition Size - size of the EFI System Partition partition
espsize='512M'

# Whether to enable a swap partition/Logical Volume
# For LVM installs, it will create a LV of this size for swap
# For ext4 filesystem without LVM, it will create a swap partition
# For BTRFS filesystem, it will create a swap partition
swapenable='true'

# If enabled, swap partition/LV size
swapsize='4G'

# Filesystem options are currently 'ext4' and 'btrfs'
archfilesystem='ext4'

# BTRFS Filesystem Label if using BTRFS
archbtrfslabel='btrfs'

# LUKS Options
# LUKS vars to control whether to use Full Disk Encryption
# If set to 'true', then passphrase needs to be specified
# A crypto key file will be created and saved in the initram
# You may want to make backup of the crypto key file.
### THIS SCRIPT DOES NOT YET SUPPORT LUKS - IT IS A WORK-IN-PROGRESS! ###
luksencrypt='false'
lukspassphrase='lukspassphrasegoeshere'

# LVM Options
# If set to 'true', use LVM for creating logical volumes, and install Arch onto
# those Logical Volumes instead of onto raw partitions. If this is not set to
# 'true', wipe all the disks and create GPT partition tables on them, but we'll
# only create our partitions and filesystems on the first disk in the 'diskdevs'
# array. LVM allows you to use all the free space on all of your disks. Without
# it, using the space on your additional disks will require you to do the
# partitioning and filesystem creation and mounting manually.
# If you set archfilesystem='btrfs', LVM is not used regardless of this setting.
lvmenable='true'

# The following variable names that start with 'lvm' are only used if the above
# 'lvmenable' variable is set to 'true' and 'archfilesystem' is not set to 'btrfs'.

# LVM Logical Volume name for swap (if swapenable set to 'true')
lvmlvswapname='swap'

# LVM Volume Group Name to create
lvmvgname='arch'

# LVM Logical Volume name for /
lvmlvrootname='root'

# Percent of Volume Group free space to use for /
lvmlvsysrootpercent='80'

# Repository Settings
# Default location of an Arch Linux repository - no need to change this unless
# you have your own internal repository since if ${automirror} is set to 'true',
# reflector will find the best repos to use.
archrepo='https://mirrors.kernel.org/archlinux/$repo/os/$arch'

# Location of the pacman mirrorlist in Arch ISO - should not need to be changed
mirrorlist='/etc/pacman.d/mirrorlist'

# Rewrite the above archrepo using reflector to find the best mirror when 'true'
automirror='true'

# Installation package options
# If you don't need all of 'base-devel', you can set this to false.
installbasedevel='true'

# Set to 'true' to install the 'yay' AUR Helper
# If you set this to 'true', then installbasedevel will be assumed set to 'true'
installyay='true'

# Install linux-zen kernel instead of linux kernel - the linux-zen kernel makes
# an interactive system more responsive.
installzen='false'

# nftables has replaced iptables - this installs nftables instead
installnftables='true'

# NetworkManager is the preferred interactive networking option
# If not set to 'true', systemd-networkd will be enabled instead,
# but you will need to configure it manually.
installnetworkmanager='true'

# If you want man pages installed
installdocs='true'

# If this is enabled, we create a symlink at /etc/resolv.conf and enable
# systemd-resolved for DNS resolution
usesystemdresolved='true'

# Compress the initram using this binary
initramcompressor='zstd'

# Names of the editor packages to install separated by a space
installeditors='vim vi'

# Additional packages to install - firewalld and bash-completion by default
additionalpackages='bash-completion firewalld'
# Additional services to enable at boot - firewalld and sshd by default
additionalservices='firewalld sshd'

# List of AUR packages you want to install.
# If you list anything here, it will assume installyay='true'
aurpackages=''

# Graphical User Interface options
# Valid values are 'false', 'kde', 'xfce', 'gnome', 'mate', or 'lxqt'
installgui='false'

# Customize the GUI with the customization script for the normal user?
customizegui='true'

# Name of kde customization script to use (located in this repo)
customscriptkde='custom-settings-kde.sh'
# Name of the XFCE customization script to use (located in this repo)
customscriptxfce='custom-settings-xfce.sh'
# Name of the Gnome customization script to use (located in this repo)
customscriptgnome='custom-settings-gnome.sh'
# Name of the MATE customization script to use (located in this repo)
customscriptmate='custom-settings-mate.sh'
# Name of the LXQt customization script to use (located in this repo)
customscriptlxqt='custom-settings-lxqt.sh'

# Packages to install if installgui is 'true'
packageskde='plasma-meta kde-applications-meta packagekit'
packagesxfce='xfce4 xfce4-goodies lightdm lightdm-gtk-greeter xorg pulseaudio pavucontrol gvfs'
packagesgnome='gnome gnome-extra gnome-themes-extra gnome-shell-extensions gnome-shell-extension-appindicator gnome-tweaks network-manager-applet'
packagesmate='mate mate-extra mate-applet-dock mate-applet-streamer network-manager-applet system-config-printer gtk-engines gtk-engine-murrine lightdm-slick-greeter lightdm-gtk-greeter'
packageslxqt='lxqt libpulse libstatgrab libsysstat lm_sensors breeze-icons oxygen-icons sddm plasma-workspace python-xdg'

# The packages to install if we are going to configure Xorg
xorgdrivers='xorg-drivers'
amddrivers='mesa lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader'
nvidiadrivers='nvidia-dkms'

###################################################################
### There is nothing to see below this line, unless you want to ###
### deal with the consequences of making changes beyond here    ###
###################################################################

_mountpath='/mnt'
_myipinfofile=$(mktemp)
_needxorgdrivers='false'

# Function definitions

# Misc functions to format output text and such
out() { printf "$1 $2\n" "${@:3}"; }
debug() { [ ${DEBUG} = 'true' ] && out "==> DEBUG:  " "$@"; }
debug "Beginning function parsing"
error() { out "==> ERROR: " "%s" "$@"; } >&2
warning() { out "==> WARNING:" "%s" "$@"; } >&2
msg() { out "==>" "%s" "$@"; }
msg2() { out "  ->" "%s" "$@"; }
die() { error "$@"; exit 1; }
ignore_error() {
  "$@" 2>/dev/null
  return 0
}
_err() { [ ! ${DRYRUN} = 'true' ] && error "$@"; [ ! ${DRYRUN} = 'true' ] && die "Aborting"; }

# This is the function that is called to call the functions that do the install
# steps
_archinstall() {
  # Here is where the functions are called. This goes through the process of
  # installing Arch Linux. This is mostly in the order specified by the Arch Wiki,
  # but I've taken the liberty to re-arrange things where it makes sense/makes
  # the install faster. The mkinitcpio executes more than once, and I've worked
  # to try to reduce how many times it gets executed by rearranging package
  # install ordering.
  msg "Installing Arch Linux"
  if [[ ! $(id -u) == '0' ]] || [[ ! -f ~/.automated_script.sh ]]; then
    _err "You must be 'root' inside of the Arch ISO in order to run this script"
  fi
  _defaultscheck || _err 'Function _defaultscheck returned an error'
  _setvars || _err 'Function _setvars returned an error'
  _getmyipinfo || _err 'Function _getmyipinfo returned an error'
  _settimedate || _err 'Function _settimedate returned an error'
  _disklist || _err 'Function _disklist returned an error'

  # Remove all data from disks, then install
  _wipealldisks || _err 'Function _wipealldisks returned an error'
  _partitiondisks || _err 'Function _partitiondisks returned an error'
  _mkfilesystems || _err 'Function _mkfilesystems returned an error'
  _mountdisks || _err 'Function _mountdisks returned an error'
  _pacman_conf_update /etc/pacman.conf || _err 'Function _pacman_conf_update returned an error'
  _usereflector || _err 'Function _usereflector returned an error'
  _installpacman || _err 'Function _installpacman returned an error'
  # Set some pacman options - Enable Color and [multilib] in the newly installed OS under ${_mountpath}
  _pacman_conf_update ${_mountpath}/etc/pacman.conf || _err 'Function _pacman_conf_update returned an error'
  _pacmansync || _err 'Function _pacmansync returned an error'
  _installbase || _err 'Function _installbase returned an error'
  _setlocaletime || _err 'Function _setlocaletime returned an error'
  _setlocale || _err 'Function _setlocale returned an error'
  _setkeymap || _err 'Function _setkeymap returned an error'

  # Write the mounted filesystems to /etc/fstab in the newly installed OS under ${_mountpath}
  # (Repeated executions of the script on a disk, even though the disk is wiped
  # every time, would yield a UUID for the /efi from the previous disk before
  # wiping. This just started happening today while working on this script, and
  # I can't explain why. Virtualization? Adding an extra 'sync' and 'partprobe'
  # to try to overcome it. Can't hurt.)
  sync
  ignore_error partprobe
  [ ! ${DRYRUN} = 'true' ] && genfstab -U ${_mountpath} >> ${_mountpath}/etc/fstab || _err 'genfstab returned an error'
  _sethostname || _err 'Function _sethostname returned an error'

  # If we're going to use LVM, we have to install the software
  # Since we want to avoid multiple mkinitcpio builds, we install it before the
  # kernel since installing the kernel triggers a mkinitcpio build
  _installlvm || _err 'Function _installlvm returned an error'

  # Although it isn't necessary to install mkinicpio explicitly, it's being done
  # here so that the config file can be customized before the kernel triggers
  # the mkinitcpio build
  _installmkinitcpio || _err 'Function _installmkinitcpio returned an error'
  _mkinitcpioconfig || _err 'Function _mkinitcpioconfig returned an error'

  # Install the GRUB Bootloader and run grub-install command as appropriate
  # for either BIOS or UEFI boot, depending on current capability of the running
  # machine.
  _installbootloader || _err 'Function _installbootloader returned an error'

  # Install btrfs packages if that filesystem was chosen
  _installbtrfs || _err 'Function _installbtrfs returned an error'

  # Install micro-code update for either Intel or AMD if we're not in a VM
  _installucode || _err 'Function _installucode returned an error'

  # base-devel is needed to provide tools for building from source
  _installbasedevel || _err 'Function _installbasedevel returned an error'

  # Unless you install an editor, there is no editor available in Arch Linux
  _installeditor || _err 'Function _installeditor returned an error'

  # Networking is usually required - we're defaulting to NetworkManager for now
  _installnetworkmanager || _err 'Function _installnetworkmanager returned an error'

  # Configure the symlink and start the systemd-resolved service
  _usesystemdresolved || _err 'Function _usesystemdresolved returned an error'

  # man pages are optional, but can come in handy
  _installdocs || _err 'Function _installdocs returned an error'
  _installssh || _err 'Function _installssh returned an error'

  # This will determine if we're running in a virtual machine and install the appropriate guest package(s)
  _installguestpkgs || _err 'Function _installguestpkgs returned an error'

  # HAVEGE entropy gathering is needed for Virtual Machines
  _installentropy || _err 'Function _installentropy returned an error'

  # If we installed a GUI, then we'll need the Xorg video drivers
  _installxorgdrivers || _err 'Function _installxorgdrivers returned an error'

  # If ${additionalpackages} is non-empty, install the packages in the list
  _installadditionalpackages || _err 'Function _installadditionalpackages returned an error'

  # If ${additionalservices} is non-empty, enable the services in the list
  _enableadditionalservices || _err 'Function _enableadditionalservices returned an error'

  # So we don't rebuild mkinicpio so many times during install, this is after
  # all other package installs since some of them trigger a mkinitcpio call
  _installkernel || _err 'Function _installkernel returned an error'

  # Now, update Grub with all the changes we have made
  _grubmkconfig || _err 'Function _grubmkconfig returned an error'

  # Add a normal user
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} useradd -m -s /bin/bash -G wheel "${username}" || _err "useradd failed to create user ${username}"

  # If ${installgui} is set to a DE name, install the chosen Desktop Environment
  _installgui || _err 'Function _installgui returned an error'

  # If ${installyay} is 'true', install the 'yay' package manager
  # This function also installs any packages in the ${aurpackages} list
  _installyay || _err 'Function _installyay returned an error'

  # If there is a value in the usersshkey variable, write it to the user authorized_keys file
  if [[ ! ${usersshkey} == '' ]]; then
    _installauthorizedkeys ${usersshkey} || _err 'Function _installauthorizedkeys returned an error'
  fi
  if [[ ! ${usersshkeylocation} == '' ]]; then
    _installauthorizedkeys $(curl -L ${usersshkeylocation}) || _err 'Function _installauthorizedkeys returned an error'
  fi

  # In the case of using ssh to connect to the Arch ISO and running this script,
  # copying the host keys to the chroot /etc/ssh/ will avoid a warning about the
  # host keys not being the same when you attempt to ssh into the newly booted OS,
  # if the newly booted OS gets the same IP address from the DHCP Server.
  _sshcopyhostkeys || _err "Function _sshcopyhostkeys returned an error"

  # Set the root and normal user passwords
  [ ! ${DRYRUN} = 'true' ] && echo "root:${rootpw}" | arch-chroot ${_mountpath} chpasswd || _err 'Failed to set root password using chpasswd'

  if [[ ${setuserpw} == 'true' ]]; then
    [ ! ${DRYRUN} = 'true' ] && echo "${username}:${userpw}" | arch-chroot ${_mountpath} chpasswd || _err 'Failed to set user password using chpasswd'
  fi
  return 0
}

# Cleanup routine - we'll add anything here that needs to be cleaned up
# at the end of a successful or aborted install
# This function is called via a trap that executes when the script exits
_cleanup() {
  debug "_cleanup function"
  msg "Cleaning up..."
  [[ -f "${_myipinfofile}" ]] && rm "${_myipinfofile}"
  [[ -f "${mirrorlist}.backup" ]] && mv ${mirrorlist}{.backup,}
  sync
  ignore_error umount -R ${_mountpath}
  swapoff -a
  return 0
}

_completionbanner() {
  debug "_completionbanner function"
  # Let the user know we're done
  echo "##############################################"
  echo "# When you see the shell prompt below, the   #"
  echo "# disk(s) has/have been unmounted.           #"
  echo "# Shut down this machine (run 'poweroff').   #"
  echo "# Remove the ISO disk from the CD-ROM drive. #"
  echo "# When you start the machine again, there is #"
  echo "# a strong possibility that it may boot into #"
  echo "# Arch Linux, BTW.                           #"
  echo "##############################################"
  return 0
}

_customizelvmconf() {
  debug "_customizelvmconf function"
  # For environments where Arch may be installed on thin-provisioned storage,
  # we need to configure LVM to issue discards to reclaim disk space
  if [[ ${_lvmenable} == 'true' ]]; then
    [ ! ${DRYRUN} = 'true' ] && sed -i 's/issue_discards = 0/issue_discards = 1/g' ${_mountpath}/etc/lvm/lvm.conf || _err "Failed to modify /etc/lvm/lvm.conf"
  fi
  return 0
}

_defaultscheck() {
  debug "_defaultscheck function"
  msg "Checking default variable values"
  # If user didn't change the default passwords - bail out
  if ([[ ${setuserpw} == 'true' ]] && [[ ${userpw} == 'userpasswordgoeshere' ]]) || [[ ${rootpw} == 'rootpasswordgoeshere' ]] || ([[ ${luksencrypt} == 'true' ]] && [[ ${lukspassphrase} == 'lukspassphrasegoeshere' ]]); then
    _err "You didn't edit the variables\n\
    Please edit this script and review the variables that \n\
    should be edited to your preferences before execution.\n"
  fi
  return 0
}

_detectamdgpu() {
  debug "_detectamdgpu function"
  lspci | grep -e VGA | grep AMD >/dev/null
}

_detectnvidiagpu() {
  debug "_detectnvidiagpu function"
  lspci | grep -e VGA | grep NVIDIA >/dev/null
}

_detectvirtualization() {
  debug "_detectvirtualization function"
  # Test for running under a virtual machine
  [[ $(systemd-detect-virt -v) != 'none' ]]
}

# If autodisklist is 'true', go find disks we can use, else use the predefined
# array
_disklist() {
  debug "_disklist function"
  msg "Get list of block storage devices we can use"
  local _blockdevice
  if [[ ${autodisklist} == 'true' ]]; then
    diskdevs=()
    for _blockdevice in $(lsblk -d -I 3,8,9,11,22,33,34,56,57,65,66,67,68,69,70,71,88,89,90,91,128,129,130,131,132,133,134,135,254,259 -l -n -o NAME -p); do
      # Detect the device from which ArchISO may have been booted so we don't erase it - ignore it
      if [[ ! $(lsblk -d -l -n -o FSTYPE -p "${_blockdevice}") == 'iso9660' ]]; then
        diskdevs+=("${_blockdevice}")
      fi
    done
  fi

  # If no disks are listed, bail out
  if [[ ${#diskdevs[@]} == '0' ]]; then
    _err "There are no disks enumerated for installation"
  fi
  debug "Detected disks ${diskdevs}"
  return 0
}

_efitest() {
  debug "_efitest function"
  # Test to see if we're running on an EFI-enabled system
  [[ -d /sys/firmware/efi/efivars ]]
}

_enableservice() {
  debug "_enableservice function"
  msg "Enabling service $@"
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} systemctl enable $@ || _err "Failed to enable service $@"
  return 0
}

_enableadditionalservices() {
  debug "_enableadditionalservices function"
  if [[ ! -z ${additionalservices} ]]; then
    _enableservice ${additionalservices} || _err "Failed to enable additional services ${additionalservices}"
  fi
  return 0
}

# Get GeoIP information to discover country and timezone
_getmyipinfo() {
  debug "_getmyipinfo function"
  msg "Retrieving GeoIP information to determine Country and timezone"
  curl http://ip-api.com/line/?fields=status,country,timezone > "${_myipinfofile}" 2>/dev/null || _err "Failed to curl IP Address information from ip-api.com"

  # The first line should say 'success', if not, bail out
  if [[ ! $(cat "${_myipinfofile}" | sed -n '1p')x == 'successx' ]]; then
    _err "Can't get ip info from ip-api.com \n$(cat ${_myipinfofile})"
  fi
  # Which country code to use for Arch Mirrors
  reflectorcountry=$(cat "${_myipinfofile}" | sed -n '2p')
  # See timedatectl list-timezones
  ltimezone=$(cat "${_myipinfofile}" | sed -n '3p')
  debug "My IP Info file contents:"
  debug "$(cat ${_myipinfofile})\n"
  msg2 "Country ${reflectorcountry}"
  msg2 "TimeZone ${ltimezone}"
  return 0
}

_grubmkconfig() {
  debug "_grubmkconfig function"
  # If ever LVM and BTRFS are used together, the following logic may need to be changed
  if [[ ${_lvmenable} == 'true' ]]; then
    # Add lvm to Grub modules - unnecessary since Grub detects this, but technically correct
    msg2 "Modify Grub to include LVM module"
    [ ! ${DRYRUN} = 'true' ] && sed -i 's@^\(GRUB_PRELOAD_MODULES=\).*@\1"part_gpt part_msdos lvm"@' ${_mountpath}/etc/default/grub || _err "Failed to modify /etc/default/grub"
  elif [[ ${archfilesystem} == 'btrfs' ]]; then
    # Add btrfs to Grub modules
    msg2 "Modify Grub to include BTRFS module"
    [ ! ${DRYRUN} = 'true' ] && sed -i 's@^\(GRUB_PRELOAD_MODULES=\).*@\1"part_gpt part_msdos btrfs"@' ${_mountpath}/etc/default/grub || _err "Failed to modify /etc/default/grub"
  fi
  if [[ ${luksencrypt} == 'true' ]]; then
    [ ! ${DRYRUN} = 'true' ] && sed -i 's@^#\(GRUB_ENABLE_CRYPTODISK=\).*@\1"y"@' ${_mountpath}/etc/default/grub || _err "Failed to modify /etc/default/grub"
  fi
  # Enable Grub menu colors
  msg2 "Modify Grub to enable menu colors"
  [ ! ${DRYRUN} = 'true' ] && sed -i 's@^#\(GRUB_COLOR_NORMAL=\).*@\1"light-blue/black"@' ${_mountpath}/etc/default/grub || _err "Failed to modify /etc/default/grub"
  [ ! ${DRYRUN} = 'true' ] && sed -i 's@^#\(GRUB_COLOR_HIGHLIGHT=\).*@\1"light-cyan/blue"@' ${_mountpath}/etc/default/grub || _err "Failed to modify /etc/default/grub"
  # Create/update the grub.cfg
  msg2 "Generating /boot/grub/grub.cfg"
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} grub-mkconfig -o /boot/grub/grub.cfg || _err "Failed to execute grub-mkconfig"
  return 0
}

_installadditionalpackages() {
  debug "_installadditionalpackages function"
  # Added test here for either installing 'yay' or AUR packages, because 'git'
  # will add a user to the passwd file, and I prefer that is done before adding
  # a regular user.
  if [[ ${installyay} == 'true' ]] || [[ ! -z ${aurpackages} ]]; then
    # git for installing AUR packages
    _installpackageasdeps git || _err "pacman failed to install 'git'"
  fi
  if [[ ! -z ${additionalpackages} ]]; then
    _installpackage ${additionalpackages} || _err "Failed to install additional packages ${additionalpackages}"
  fi
  return 0
}

_installauthorizedkeys() {
  debug "_installauthorizedkeys function"
  local authorizedkeys2
  authorizedkeys2="${@}"
  if (echo ${authorizedkeys2} | ssh-keygen -lf -); then
    [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} su --login "${username}" -c "cd ~; [ ! -d .ssh ] && mkdir .ssh; chmod 700 .ssh; [ ! -f .ssh/authorized_keys ] && touch .ssh/authorized_keys; chmod 644 .ssh/authorized_keys; printf \"%s\n\" \"${authorizedkeys2}\" >> .ssh/authorized_keys;" || _err "Failed to write user ssh key to authorized_keys"
  else
    _err "Bad SSH public key format"
  fi
  return 0
}

_installbase() {
  debug "_installbase function"
  # install base meta-package
  msg "Installing base meta-package"
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} pacman --quiet --noconfirm --disable-download-timeout -S base || _err "pacman failed to install 'base'"
  return 0
}

_installbasedevel() {
  debug "_installbasedevel function"
  if [[ ${installbasedevel} == 'true' || ${installyay} == 'true' ]]; then
    # base-devel for building
    _installpackage base-devel || _err "pacman failed to install 'base-devel'"
    _sudoersconfig || _err 'Function _sudoersconfig returned an error'
  fi
  return 0
}

_installbootloader() {
  debug "_installbootloader function"
  # grub for Boot Loader
  # efibootmgr required for EFI
  if _efitest; then
    msg2 "UEFI System detected"
    _installpackage grub efibootmgr || _err "pacman failed to install 'grub efibootmgr'"
    # Install Grub Boot Loader to the EFI System Partition
    [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} grub-install --target=x86_64-efi --efi-directory=/efi --removable || _err "Failed to execute grub-install to EFI System Partition"
  else
    msg2 "BIOS System detected"
    _installpackage grub || _err "pacman failed to install 'grub'"
    [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} grub-install --target=i386-pc ${firstdisk} || _err "Failed to execute grub-install on ${firstdisk} non-EFI"
  fi
  return 0
}

_installbtrfs() {
  debug "_installbtrfs function"
  if [[ ${archfilesystem} == 'btrfs' ]]; then
    # btrfs packages because we're using btrfs filesystem
    _installpackage btrfs-progs compsize btrfs-heatmap duperemove grub-btrfs || _err "pacman failed to install btrfs related packages"
  fi
  return 0
}

_installdocs() {
  debug "_installdocs function"
  # man pages
  if [[ ${installdocs} == 'true' ]]; then
    _installpackage man-db man-pages || _err "pacman failed to install 'man-db man-pages'"
  fi
  return 0
}

_installeditor() {
  debug "_installeditor function"
  # Selected text editors
  _installpackage ${installeditors} || _err "pacman failed to install '${installeditors}'"
  return 0
}

_installentropy() {
  debug "_installentropy function"
  # haveged should be installed on all Virtual Machines to gather entropy
  if _detectvirtualization; then
    msg2 "Virtual Machine detected"
    _installpackage haveged || _err "pacman failed to install 'haveged'"
    _enableservice haveged || _err "Failed to enable haveged service"
  fi
  return 0
}

_installguestpkgs() {
  debug "_installguestpkgs function"
  if _detectvirtualization; then
    if _virtualboxenv; then
      # Install virtualbox guest using dkms (kernel headers needed for dkms)
      _installpackage virtualbox-guest-dkms virtualbox-guest-utils "${installkernel}"-headers || _err "pacman failed to install 'virtualbox-guest-dkms virtualbox-guest-utils'"
      _enableservice vboxservice || _err "Failed to enable vboxservice service"
    elif _vmwareenv; then
      # Install vmware-tools
      _installpackage open-vm-tools xf86-video-vmware xf86-input-vmmouse || _err "pacman failed to install 'open-vm-tools xf86-video-vmware xf86-input-vmmouse'"
      _enableservice vmware-vmblock-fuse || _err "Failed to enable vmware-vmblock-fuse service"
    elif _qemuenv; then
      # Install qemu-guest-agent
      _installpackage qemu-guest-agent || _err "pacman failed to install 'qemu-guest-agent'"
    fi
  fi
  return 0
}

_installgui() {
  debug "_installgui function"
  local _guidm
  local _guipackages
  local _guicustomscript
  local _installguipackages
  local _enabledm
  case ${installgui} in
    kde)
      msg2 "Selected KDE Plasma GUI"
      _guidm='sddm'
      _needxorgdrivers='true'
      _guipackages=${packageskde}
      _guicustomscript=${customscriptkde}
      _installguipackages='true'
      _enabledm='true'
      ;;
    xfce)
      msg2 "Selected XFCE GUI"
      _guidm='lightdm'
      _needxorgdrivers='true'
      _guipackages=${packagesxfce}
      _guicustomscript=${customscriptxfce}
      _installguipackages='true'
      _enabledm='true'
      ;;
    gnome)
      msg2 "Selected GNome GUI"
      _guidm='gdm'
      _needxorgdrivers='true'
      _guipackages=${packagesgnome}
      _guicustomscript=${customscriptgnome}
      _installguipackages='true'
      _enabledm='true'
      ;;
    mate)
      msg2 "Selected MATE GUI"
      _guidm='lightdm'
      _needxorgdrivers='true'
      _guipackages=${packagesmate}
      _guicustomscript=${customscriptmate}
      _installguipackages='true'
      _enabledm='true'
      ;;
    lxqt)
      msg2 "Selected LXQt GUI"
      _guidm='sddm'
      _needxorgdrivers='true'
      _guipackages=${packageslxqt}
      _guicustomscript=${customscriptlxqt}
      _installguipackages='true'
      _enabledm='true'
      ;;
  esac
  if [[ ${_installguipackages} == 'true' ]]; then
    _installpackage ${_guipackages} || _err "pacman failed to install ${_guipackages}"
    if [[ ${_enabledm} == 'true' ]]; then
      _enableservice ${_guidm} || _err "Failed to enable ${_guidm} service"
    fi
    if [[ ${customizegui} == 'true' ]]; then
      msg2 "Executing ${_guicustomscript} as the user ${username}"
      [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} su --login "${username}" -c "cd ~; curl -L -o ${_guicustomscript} ${archinstallrepo}/${_guicustomscript}; chmod u+x ${_guicustomscript}; ./${_guicustomscript}; rm ${_guicustomscript}" || _err "Failed to download or execute ${archinstallrepo}/${_guicustomscript}"
    fi
  fi
  return 0
}

_installkernel() {
  debug "_installkernel function"
  # linux-zen or linux for a kernel
  # linux-firmware (not really necessary in virtualization, but probably needed on bare metal)
  if _detectvirtualization; then
    msg2 "Detected Virtual Machine Environment"
    _installpackage ${installkernel} || _err "pacman failed to install ${installkernel}"
  else
    _installpackage ${installkernel} linux-firmware || _err "pacman failed to install '${installkernel} linux-firmware'"
  fi
  return 0
}

_installlvm() {
  debug "_installlvm function"
  # lvm2 because we're using LVM for disk management
  if [[ ${_lvmenable} == 'true' ]]; then
    _installpackage lvm2 || _err "pacman failed to install 'lvm2'"
    if _isssd ${firstdisk}; then
      msg2 "Detected SSD/NVMe - customizing LVM for discards"
      _customizelvmconf || _err 'Function _customizelvmconf returned an error'
    fi
  fi
  return 0
}

_installmkinitcpio() {
  debug "_installmkinitcpio function"
  # mkinitcpio
  _installpackageasdeps mkinitcpio || _err "pacman failed to install 'mkinitcpio'"
  return 0
}

_installnetworkmanager() {
  debug "_installnetworkmanager function"
  # networkmanager for Networking
  if [[ ${installnetworkmanager} == 'true' ]]; then
    _installpackage networkmanager || _err "pacman failed to install 'networkmanager'"
    _enableservice NetworkManager || _err "Failed to enable NetworkManager service"
  else
    _enableservice systemd-networkd || _err "Failed to enable systemd-networkd service"
  fi
  return 0
}

_installpackage() {
  debug "_installpackage function"
  msg "Installing package $@"
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} pacman --needed --quiet --noconfirm --disable-download-timeout -S "$@" || _err 'Failed to install packages: %s' "$@"
  return 0
}

_installpackageasdeps() {
  debug "_installpackageasdeps function"
  msg "Installing package $@ as dependency"
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} pacman --needed --quiet --asdeps --noconfirm --disable-download-timeout -S "$@" || _err 'Failed to install packages: %s' "$@"
  return 0
}

_installpacman() {
  debug "_installpacman function"
  # install pacman in the chroot so we can use it to install other packages inside the chroot
  # we're going to use pacman's ability to not re-install packages already installed using --needed
  msg "Installing pacman package via pacstrap to ${_mountpath}"
  [ ! ${DRYRUN} = 'true' ] && pacstrap ${_mountpath} pacman || _err "pacstrap failed to install 'pacman' at ${_mountpath}"
  return 0
}

_installssh() {
  debug "_installssh function"
  # openssh
  _installpackage openssh || _err "pacman failed to install 'openssh'"
  return 0
}

_installucode() {
  debug "_installucode function"
  # Install ucode package for the processor we're using
  if ! _detectvirtualization; then
    if (grep ^vendor_id /proc/cpuinfo | grep AuthenticAMD >/dev/null); then
      msg "Detected AMD CPU"
      _installpackage amd-ucode || _err "pacman failed to install 'amd-ucode'"
    elif (grep ^vendor_id /proc/cpuinfo | grep Intel >/dev/null); then
      msg "Detected Intel CPU"
      _installpackage intel-ucode || _err "pacman failed to install 'intel-ucode'"
    fi
  fi
  return 0
}

_installxorgdrivers() {
  debug "_installxorgdrivers function"
  if [[ ${_needxorgdrivers} == 'true' ]]; then
    # Installing nVidia package when we detect nVidia graphics - only works for currently supported cards
    _installpackage ${xorgdrivers} || _err "pacman failed to install ${xorgdrivers}"
    _detectnvidiagpu && _installpackage ${nvidiadrivers} || true
    _detectamdgpu && _installpackage ${amddrivers} || true
  fi
  return 0
}

_installyay() {
  debug "_installyay function"
  # 'base-devel' is required to install 'yay'
  # To prevent confusion, we purposefully set the var to 'true' and call the
  # function to install 'base-devel' when 'installyay' is 'true'
  if [[ ${installyay} == 'true' ]] || [[ ! -z ${aurpackages} ]]; then
    # Download and install yay, then clean up by removing the source directory
    [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} su --login "${username}" -c 'cd ~; git clone https://aur.archlinux.org/yay-bin.git; cd ~/yay-bin; makepkg -sicr --noconfirm; cd ~; sudo rm -R yay-bin' || _err "Failed to clone/build/install yay"
    # Use the newly installed 'yay' package to install packages from the AUR
    if [[ ! -z ${aurpackages} ]]; then
      [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} su --login "${username}" -c "yay --needed --noconfirm --color always --quiet -S ${aurpackages}" || _err "Failed to install additional packages ${aurpackages}"
    fi
  fi
  return 0
}

_isssd() {
  debug "_isssd function"
  local _devicename
  _devicename=$(basename ${1})
  [[ $(cat /sys/block/${_devicename}/queue/rotational) == '0' ]]
}

_lvcreate() {
  debug "_lvcreate function"
  local _lvsize="${1}"
  local _lvname="${2}"
  local _vgname="${3}"
  local _sizeflag
  if [[ ${_lvsize} =~ '%FREE' ]]; then
    _sizeflag="-l"
  else
    _sizeflag="--size"
  fi
  if [[ ! $(lvs --noheadings | egrep "\ ${_lvname}\ ") ]]; then
    msg "Creating Logical Volume ${_lvname} in Volume Group ${_vgname} of size ${_lvsize}"
    [ ! ${DRYRUN} = 'true' ] && lvcreate -q -W y -y "${_sizeflag}" "${_lvsize}" --name "${_lvname}" "${_vgname}" >/dev/null || _err "Failed to create Logical Volume ${_lvname} in Volume Group ${_vgname}"
  else
    _err "Logical Volume ${_lvname} already exists"
  fi
  return 0
}

_mkfilesystems() {
  debug "_mkfilesystems function"
  [[ ! -d ${_mountpath} ]] && mkdir -p ${_mountpath}
  [[ -d ${_mountpath} ]] || _err "Unable to ensure ${_mountpath} exists"
  if _efitest; then
    # Format the EFI System Partition as FAT32
    msg2 "Formatting ${esppartname} as EFI System Partition using FAT32"
    [ ! ${DRYRUN} = 'true' ] && mkfs.vfat -F32 "${esppartname}" >/dev/null || _err "mkfs.vfat on ${esppartname} failed!"
  fi
  case ${archfilesystem} in
    ext4)
      msg2 "Using ${archfilesystem} as the filesystem"
      if [[ ${_lvmenable} == 'true' ]]; then
        # Format the / as ext4
        msg2 "Formatting ${lvmvgname}/${lvmlvrootname} as / LV using ${archfilesystem}"
        [ ! ${DRYRUN} = 'true' ] && mkfs.ext4 -Fq -L 'root' /dev/mapper/"${lvmvgname}"'-'"${lvmlvrootname}" >/dev/null || _err "mkfs.ext4 on /dev/mapper/${lvmvgname}-${lvmlvrootname} failed!"
        if [[ ${swapenable} == 'true' ]]; then
          msg2 "${lvmvgname}/${lvmlvswapname}: Executing mkswap"
          [ ! ${DRYRUN} = 'true' ] && mkswap -f /dev/mapper/${lvmvgname}-${lvmlvswapname} >/dev/null || _err "Failed to mkswap on /dev/mapper/${lvmvgname}-${lvmlvswapname}"
        fi
      else
        # Format the / as ext4
        msg2 "Formatting ${rootpartname} as / using ${archfilesystem}"
        [ ! ${DRYRUN} = 'true' ] && mkfs.ext4 -Fq -L 'root' "${rootpartname}" >/dev/null || _err "mkfs.ext4 on ${rootpartname} failed!"
        if [[ ${swapenable} == 'true' ]]; then
          msg2 "Executing mkswap on: ${_swappartname}"
          [ ! ${DRYRUN} = 'true' ] && mkswap -f ${_swappartname} >/dev/null 2>&1 || _err "Failed to mkswap on ${_swappartname}"
        fi
      fi
      ;;
    btrfs)
      if [[ ${swapenable} == 'true' ]]; then
        msg2 "Executing mkswap on: ${_swappartname}"
        [ ! ${DRYRUN} = 'true' ] && mkswap -f ${_swappartname} >/dev/null 2>&1 || _err "Failed to mkswap on ${_swappartname}"
      fi
      msg2 "Formatting ${rootpartname} as ${archfilesystem}"
      [ ! ${DRYRUN} = 'true' ] && mkfs.btrfs -fq -L "${archbtrfslabel}" "${rootpartname}" || _err "mkfs.btrfs on ${rootpartname} failed!"
      [ ! ${DRYRUN} = 'true' ] && mount -o rw,noatime,space_cache=v2,compress=zstd:3,autodefrag,subvolid=5 "${rootpartname}" ${_mountpath}
      # The opinion of how to setup subvolumes varies. For compatibility with
      # timeshift and snapper, using '@' for '/' and '@home' for '/home'
      # Using subvol name = filesystem directory name for the remaining
      # subvolumes for simplicity.
      msg2 "Creating ${archfilesystem} subvolumes"
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@ >/dev/null
      [ ! ${DRYRUN} = 'true' ] && mkdir -p ${_mountpath}/@/home >/dev/null || _err "Creating Directory /@/home at ${_mountpath} failed!"
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@home >/dev/null
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@/root >/dev/null
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@/srv >/dev/null
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@/var >/dev/null
      [ ! ${DRYRUN} = 'true' ] && mkdir -p ${_mountpath}/@/usr >/dev/null || _err "Creating Directory /@/usr at ${_mountpath} failed!"
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@/usr/local >/dev/null
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@/opt >/dev/null
      [ ! ${DRYRUN} = 'true' ] && mkdir -p ${_mountpath}/@/boot/grub >/dev/null || _err "Creating Directory /@/boot/grub at ${_mountpath} failed!"
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@/boot/grub/x86_64-efi >/dev/null
      [ ! ${DRYRUN} = 'true' ] && btrfs su cr ${_mountpath}/@/boot/grub/i386-pc >/dev/null
      [ ! ${DRYRUN} = 'true' ] && umount ${_mountpath}
      ;;
    *)
      _err "Variable 'archfilesystem' must be either 'ext4' or 'btrfs'"
      ;;
  esac
  return 0
}

_mkinitcpioconfig() {
  debug "_mkinitcpioconfig function"
  msg "Modify mkinicpio.conf for systemd"
  [ ! ${DRYRUN} = 'true' ] && sed -i 's/base\ udev/base systemd udev/' ${_mountpath}/etc/mkinitcpio.conf || _err "Failed to modify mkinitcpio.conf"
  if [[ ${_lvmenable} == 'true' ]]; then
    # Some mkinicpio hooks need to have tweaks for LVM
    msg "Modify mkinicpio.conf for LVM"
    [ ! ${DRYRUN} = 'true' ] && sed -i 's/block\ filesystems/block lvm2 filesystems/' ${_mountpath}/etc/mkinitcpio.conf || _err "Failed to modify mkinitcpio.conf"
  elif [[ ${archfilesystem} == 'btrfs' ]]; then
    msg "Modify mkinicpio.conf for BTRFS"
    [ ! ${DRYRUN} = 'true' ] && sed -i 's/\ fsck//' ${_mountpath}/etc/mkinitcpio.conf || _err "Failed to modify mkinitcpio.conf"
  fi

  # Compress the initram using ${initramcompressor}
  msg "Modify mkinitcpio.conf for compressor ${initramcompressor}"
  [ ! ${DRYRUN} = 'true' ] && sed -i "s/^#\(COMPRESSION=\"${initramcompressor}\"\)/\1/" ${_mountpath}/etc/mkinitcpio.conf || _err "Failed to modify mkinitcpio.conf"
  return 0
}

_mountdisks() {
  debug "_mountdisks function"
  local mountoptions
  local mountbootoptions
  # We only test the first disk to determine if it is a SSD
  # If there are multiple devices and they are a mix between ssd and non-ssd,
  # then you shouldn't use autodisklist='true', but instead list the devices
  # in the diskdevs array. We don't touch disks other than the ones in the
  # array when autodisklist is not set to 'true'.
  if [[ ${archfilesystem} == 'ext4' ]]; then
    if _isssd ${firstdisk}; then
      msg2 "Setting discard mount option on SSD/NVMe for ${firstdisk}"
      mountoptions='-o discard'
      mountbootoptions='-o discard'
    fi
    if [[ ${_lvmenable} == 'true' ]]; then
      # Mount the filesystems to ${_mountpath}
      msg2 "Mounting LVM Logical Volumes"
      [ ! ${DRYRUN} = 'true' ] && mount ${mountoptions} /dev/mapper/"${lvmvgname}"'-'"${lvmlvrootname}" ${_mountpath} >/dev/null || _err "Mounting Logical Volume ${lvmvgname}/${lvmlvrootname} at ${_mountpath} failed!"
      if [[ ${swapenable} == 'true' ]]; then
        msg2 "Enabling SWAP on: ${lvmvgname}/${lvmlvswapname}"
        [ ! ${DRYRUN} = 'true' ] && swapon -d /dev/mapper/${lvmvgname}-${lvmlvswapname} >/dev/null || _err "Failed to activate swap on /dev/mapper/${lvmvgname}-${lvmlvswapname}"
      fi
    else
      msg2 "Mounting filesystems"
      [ ! ${DRYRUN} = 'true' ] && mount ${mountoptions} ${rootpartname} ${_mountpath} >/dev/null || _err "Mounting ${rootpartname} at ${_mountpath} failed!"
      if [[ ${swapenable} == 'true' ]]; then
        msg2 "Enabling SWAP on: ${_swappartname}"
        [ ! ${DRYRUN} = 'true' ] && swapon -d ${_swappartname} >/dev/null || _err "Failed activate swap partition on ${_swappartname}"
      fi
    fi
    if _efitest; then
      msg2 "Mounting EFI System Partition"
      [ ! ${DRYRUN} = 'true' ] && mkdir ${_mountpath}/efi >/dev/null || _err "Creating Directory efi/ at ${_mountpath} failed!"
      [ ! ${DRYRUN} = 'true' ] && mount "${esppartname}" ${_mountpath}/efi >/dev/null || _err "Mounting EFI System Partition ${esppartname} at ${_mountpath}/efi failed!"
    fi
  fi
  if [[ ${archfilesystem} == 'btrfs' ]]; then
    if _isssd ${firstdisk}; then
      msg2 "Setting discard mount option on SSD/NVMe for ${firstdisk}"
      mountoptions='-o rw,noatime,ssd,space_cache=v2,compress=zstd:3,autodefrag'
      mountbootoptions='-o discard'
    else
      mountoptions='-o rw,noatime,space_cache=v2,compress=zstd:3,autodefrag'
    fi
    msg2 "Mounting BTRFS '@' subvolume on /"
    [ ! ${DRYRUN} = 'true' ] && mount ${mountoptions} -o subvol=/@ ${rootpartname} ${_mountpath}
    msg2 "Mounting BTRFS subvolume '@home' on /home"
    [ ! ${DRYRUN} = 'true' ] && mount ${mountoptions} -o subvol=@home ${rootpartname} ${_mountpath}/home >/dev/null || _err "Mounting ${rootpartname} subvolume @home at ${_mountpath}/home failed!"
    if _efitest; then
      msg2 "Mounting EFI System Partition"
      [ ! ${DRYRUN} = 'true' ] && mkdir ${_mountpath}/efi >/dev/null || _err "Creating Directory efi/ at ${_mountpath} failed!"
      [ ! ${DRYRUN} = 'true' ] && mount "${esppartname}" ${_mountpath}/efi >/dev/null || _err "Mounting EFI System Partition ${esppartname} at ${_mountpath}/efi failed!"
    fi
    if [[ ${swapenable} == 'true' ]]; then
      msg2 "Enabling SWAP on: ${_swappartname}"
      [ ! ${DRYRUN} = 'true' ] && swapon -d ${_swappartname} >/dev/null || _err "Failed to activate swap partition on ${_swappartname}"
    fi
  fi
  return 0
}

_pacmansync() {
  debug "_pacmansync function"
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} pacman -Syy || _err 'Failed to sync pacman db'
  return 0
}

_pacman_conf_update() {
  debug "_pacman_conf_update function"
  local _pacman_conf="${1}"
  # Set some pacman options - Enable Color and [multilib]
  msg "Customizing ${_pacman_conf}"
  [ ! ${DRYRUN} = 'true' ] && sed -i 's/^#\(Color\)/\1/' "${_pacman_conf}" || _err "Failed to modify ${_pacman_conf}"
  [ ! ${DRYRUN} = 'true' ] && sed -i 's/^#\(VerbosePkgLists\)/\1/' "${_pacman_conf}" || _err "Failed to modify ${_pacman_conf}"
  [ ! ${DRYRUN} = 'true' ] && sed -i 's/^#\(ParallelDownloads\ .\+\)/\1/' "${_pacman_conf}" || _err "Failed to modify ${_pacman_conf}"
  [ ! ${DRYRUN} = 'true' ] && sed -i '/^#\[multilib\]/{
    N
    s/^#\(\[multilib\]\n\)#\(Include\ .\+\)/\1\2/
  }' "${_pacman_conf}" || _err "Failed to modify ${_pacman_conf}"
  return 0
}

_partitiondisks() {
  debug "_partitiondisks function begin"
  # This set of parted commands initializes every disk found in the system if
  # autodisklist='true', else only the disks listed in the diskdevname array.
  # It creates a new partition table using the GPT partition type, creates a
  # BIOS_Boot partition, and ESP on the first disk (this covers all use cases,
  # UEFI or CSM), then creates the LVM partition. Only the first disk gets an
  # ESP, the rest of the disks (if any) do not. The BIOS Boot partition is never
  # formatted or used except by Grub. Even though the ESP isn't used by non-UEFI
  # systems, it's better to have it and not need it, in case you move your hard
  # drive(s) to a newer machine and then need an ESP in order to boot. (You would
  # need to install Grub to the ESP in this case, as we don't do the ESP install
  # if you're not currently using UEFI.)

  # This looks complicated, is hard to debug, but it's a flow of logic that
  # follows this thought process:
  # We have an array of block devices ${diskdevs[@]} we either get from the user
  # variable at the top of the file, or if autodisklist='true', we get a list
  # of all block devices from lsblk.
  # We loop through each disk in the array and perform everything on that device
  # during a single pass through the loop.
  # Actions are going to depend on whether we have ${lvmenable} set to 'true'
  # If we are using LVM
  #   Add a LVM partition
  #   pvcreate the LVM partition
  #   If this is the first disk
  #     create a new Volume Group
  #   Else this is not the first disk
  #     extend the existing Volume Group by adding the Physical Volume to it.
  #     (_vgadd function determines this when called)
  #   Create the Logical Volumes
  # Else we are not using LVM
  #   All operations will be done only on the first disk
  # Format the Partitions/Logical Volumes
  # Mount the Partitions/Logical Volumes

  firstdisk="${diskdevs[0]}"
  additionaldiskdevs=${diskdevs}
  unset additionaldiskdevs[0]
  local _targetdisk
  # local _swappartname

  for _targetdisk in "${diskdevs[@]}"; do
    # BIOS Boot 21686148-6449-6E6F-744E-656564454649 = gptdisk 2-byte code EF02
    msg "Creating BIOS Boot partition on: ${_targetdisk}"
    [ ! ${DRYRUN} = 'true' ] && sgdisk -a 2 -n 1:34:2047 -t 1:EF02 -c 1:'BIOS Boot' "${_targetdisk}" >/dev/null || _err "Failed to create BIOS Boot partition on ${_targetdisk}"

    if _efitest && [[ ${_targetdisk} == ${firstdisk} ]]; then
      # EFI System Partition C12A7328-F81F-11D2-BA4B-00A0C93EC93B = gptdisk 2-byte code EF00
      msg "Creating EFI System Partition on: ${_targetdisk}"
      [ ! ${DRYRUN} = 'true' ] && sgdisk -n 0:0:+"${espsize}" -t 0:EF00 -c 0:'ESP' "${_targetdisk}" >/dev/null || _err "Failed to create EFI System Partition on ${_targetdisk}"
      esppartname=$(sfdisk -d "${_targetdisk}" | grep 'type=C12A7328-F81F-11D2-BA4B-00A0C93EC93B' | awk '{ print $1 }')
    fi

    if [[ ${_lvmenable} == 'true' ]]; then
      # If we're using LVM, create an LVM partition, else we don't do anything
      # with the additional disks other than wipe them and create 'BIOS Boot'
      # partitions
      msg "Creating LVM partition on: ${_targetdisk}"
      [ ! ${DRYRUN} = 'true' ] && sgdisk -n 0:0:0 -t 0:8E00 -c 0:'LVM' "${_targetdisk}" >/dev/null || _err "Failed to create LVM Partition on ${_targetdisk}"
      lvmpartname=$(sfdisk -d "${_targetdisk}" | grep 'type=E6D6D379-F507-44C2-A23C-238F2A3DF928' | awk '{ print $1 }')
      _pvcreate "${lvmpartname}" || _err "Failed to create Physical LVM Volume on ${_targetdisk}"
      _vgadd "${lvmvgname}" "${lvmpartname}" || _err 'Function _vgadd returned an error'
    elif [[ ${_targetdisk} == ${firstdisk} ]]; then
      # Linux x86_64 root 4f68bce3-e8cd-4db1-96e7-fbcaf984b709 = gptdisk 2-byte code 8304
      msg "Creating Linux Root Partition on: ${_targetdisk}"
      [ ! ${DRYRUN} = 'true' ] && sgdisk -n 0:0:-"${_swapsize}" -t 0:8304 -c 0:'root' "${_targetdisk}" >/dev/null || _err "Failed to create Linux Root Partition on ${_targetdisk}"
      rootpartname=$(sfdisk -d "${_targetdisk}" | grep 'type=4F68BCE3-E8CD-4DB1-96E7-FBCAF984B709' | awk '{ print $1 }')
      if [[ ${swapenable} == 'true' ]]; then
        # Linux swap 0657fd6d-a4ab-43c4-84e5-0933c84b4f4f = gptdisk 2-byte code 8200
        msg "Creating SWAP partition on: ${_targetdisk}"
        [ ! ${DRYRUN} = 'true' ] && sgdisk -n 0:0:0 -t 0:8200 -c 0:'swap' "${_targetdisk}" >/dev/null || _err "Failed to create Linux SWAP Partition on ${_targetdisk}"
        _swappartname=$(sfdisk -d "${_targetdisk}" | grep 'type=0657FD6D-A4AB-43C4-84E5-0933C84B4F4F' | awk '{ print $1 }')
      fi
    fi
  done
  if [[ ${_lvmenable} == 'true' ]]; then
    # Start creating the Logical Volumes across the Volume Group
    # If swapenable='true' then create the swaplv and enable it
    # Since creating the sysroot uses a percentage of the free partition space,
    # creating the swap LV must happen first.
    if [[ ${swapenable} == 'true' ]]; then
      _lvcreate "${_swapsize}" "${lvmlvswapname}" "${lvmvgname}" || _err "Logical Volume creation of ${lvmvgname}/${lvmlvswapname} in Volume Group ${lvmvgname} failed!"
    fi
    # Create a new LVM Logical Volume using a percentage of free Volume Group space
    _lvcreate "${lvmlvsysrootpercent}"'%FREE' "${lvmlvrootname}" "${lvmvgname}" || _err 'Function _lvcreate returned an error'
  fi
  ignore_error partprobe
  return 0
}

_pvcreate() {
  debug "_pvcreate function"
  local _pvname="${1}"
  if [[ ! $(pvs --noheadings | egrep "\ ${_pvname}\ ") ]]; then
    msg2 "Adding LVM PV signature to ${_pvname}"
    [ ! ${DRYRUN} = 'true' ] && pvcreate -q -y -ff "${_pvname}" >/dev/null || _err "Failed to create Physical Volume ${_pvname}"
  else
    _err "Disk ${_pvname} is already a Physical Volume"
  fi
  return 0
}

_qemuenv() {
  debug "_qemuenv function"
  # Test for running under QEMU/KVM
  [[ $(systemd-detect-virt -v) == 'kvm' ]] || [[ $(systemd-detect-virt -v) == 'qemu' ]]
}

_sethostname() {
  debug "_sethostname function"
  # Set hostname for the new VM. Change this as you desire.
  msg2 "Setting hostname to ${archhostname}"
  [ ! ${DRYRUN} = 'true' ] && printf '%s' "${archhostname}" > ${_mountpath}/etc/hostname || _err "Failed to modify /etc/hostname"
  [ ! ${DRYRUN} = 'true' ] && cat <<-EOF > ${_mountpath}/etc/hosts || _err "Failed to modify /etc/hosts"
127.0.0.1	localhost
::1		localhost
127.0.1.1	${archhostname}.localdomain	${archhostname}
EOF
#  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} hostnamectl hostname ${archhostname} || _err "Failed to set hostname using hostnamectl"
  return 0
}

_setkeymap() {
  debug "_setkeymap function"
  # Set KEYMAP
  msg2 "Setting keymap to ${archkeymap}"
  [ ! ${DRYRUN} = 'true' ] && printf 'KEYMAP=%s' "${archkeymap}" > ${_mountpath}/etc/vconsole.conf || _err "Failed to write to /etc/vconsole.conf"
  return 0
}

_setlocale() {
  debug "_setlocale function"
  # Set locale
  msg2 "Setting locale ${archlocale}"
  [ ! ${DRYRUN} = 'true' ] && sed -i "s/^#\(${archlocale}\)/\1/" ${_mountpath}/etc/locale.gen || _err "Failed to modify locale.gen"
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} locale-gen || _err "Failed to generate locales"
  [ ! ${DRYRUN} = 'true' ] && printf 'LANG=%s' "${archlocale}" > ${_mountpath}/etc/locale.conf || _err "Failed to modify locale.conf"
  return 0
}

_setlocaletime() {
  debug "_setlocaletime function"
  # Set Time Zone
  msg2 "Setting locale time zone ${ltimezone}"
  [ ! ${DRYRUN} = 'true' ] && arch-chroot ${_mountpath} ln -sf ../usr/share/zoneinfo/"${ltimezone}" /etc/localtime || _err "Failed to create symlink for /etc/localtime to ${ltimezone}"
  return 0
}

_settimedate() {
  debug "_settimedate function"
  # Set clock via NTP - required for TLS connections to repositories via HTTPS
  msg2 "Setting host timezone to ${ltimezone}"
  [ ! ${DRYRUN} = 'true' ] && timedatectl set-timezone "${ltimezone}" || _err "Failed to set time zone to ${ltimezone}"
  msg2 "Enabling host clock sync"
  [ ! ${DRYRUN} = 'true' ] && timedatectl set-ntp true || _err "Failed to enable ntp time sync"
  return 0
}

_setvars() {
  debug "_setvars function"
  # Set MAKEFLAGS to equal number of procs for building
  export MAKEFLAGS=-j$(nproc)

  # Set package to install based on whether we set installzen to 'true'
  if [[ ${installzen} == 'true' ]]; then
    installkernel='linux-zen'
  else
    installkernel='linux'
  fi
  # Set _lvmenable to 'false' if we're using btrfs and use this var in the
  # script to determine whether lvm is really being used.
  if [[ ${archfilesystem} == 'btrfs' ]]; then
    _lvmenable='false'
  elif [[ ${lvmenable} == 'true' ]]; then
    _lvmenable='true'
  fi
  if [[ ${swapenable} == 'true' ]]; then
    _swapsize=${swapsize}
  else
    _swapsize='0'
  fi
  return 0
}

_sshcopyhostkeys() {
  debug "_sshcopyhostkeys function"
  [ ! ${DRYRUN} = 'true' ] && cp /etc/ssh/ssh_host* ${_mountpath}/etc/ssh/ || _err "Failed to copy ssh host keys to chroot /etc/ssh"
  return 0
}

_sudoersconfig() {
  debug "_sudoersconfig function"
  if [[ -f ${_mountpath}/etc/sudoers ]]; then
    # All members of the 'wheel' group can sudo without a password
    msg2 "Modifying sudoers file to allow user in wheel group to use 'sudo' passwordless"
    [ ! ${DRYRUN} = 'true' ] && sed -Ei 's/^#\s?(%wheel\sALL=\(ALL(:ALL)?\)\sNOPASSWD:\sALL)\s?/\1/' /etc/sudoers ${_mountpath}/etc/sudoers || _err "Failed to modify /etc/sudoers with sed"
  fi
  if [[ -d ${_mountpath}/etc/sudoers.d ]]; then
    msg2 "Adding ${username} to sudoers with full passwordless privileges"
    [ ! ${DRYRUN} = 'true' ] && cat <<-EOF > ${_mountpath}/etc/sudoers.d/${username} || _err "Failed to write sudoers.d user file"
${username} ALL=(ALL:ALL) NOPASSWD: ALL
EOF
    [ ! ${DRYRUN} = 'true' ] && chmod 400 ${_mountpath}/etc/sudoers.d/${username} || _err "Failed to chmod 400 the sudoers.d user file"
  fi
  return 0
}

_usage() {
  debug "_usage function"
  msg "Usage: $(basename ${0})"
}

_usereflector() {
  debug "_usereflector function"
  # NOTE: Recent versions of arch-iso have begun running reflector at boot.
  # This function adds some additional options, like prefering our own
  # country's mirrors, and https over http if available.
  # Inserting our own preferred mirror may become optional in the future.
  # Prefix our desired repo from which to retrieve Arch packages.
  # We also keep the existing mirrorlist to use as a fallback.
  msg2 "Backup mirrorlist from ArchISO"
  if [ ! -f ${mirrorlist}.backup ]; then
    [ ! ${DRYRUN} = 'true' ] && cp ${mirrorlist}{,.backup}
  fi
  msg2 "Adding ${archrepo} to ${mirrorlist}"
  printf 'Server = %s\n' "${archrepo}" > "${mirrorlist}"

  # Use reflector to perform automatic mirrorlist update if the option automirror='true' is set above
  if [[ ${automirror} == 'true' ]]; then
    # There's no guarantee there is a mirror in the ${reflectorcountry} we detected, so we'll retry without specifying a country if we get an error
    msg2 "Executing reflector to update ${mirrorlist}"
    [ ! ${DRYRUN} = 'true' ] && reflector --country "${reflectorcountry}" --sort score --age 24 --fastest 20 -p https --save "${mirrorlist}.reflector" || reflector --sort score --age 24 --fastest 15 -p https --save "${mirrorlist}.reflector" || reflector --sort score --age 24 --fastest 15 -p https -p http --save "${mirrorlist}.reflector"
    # Test to be sure we have at least one mirror in the /etc/pacman.d/mirrorlist.reflector file
    # If so, then move the file into place, else, ignore it and use the one we've already modified.
    [[ $(grep ^Server "${mirrorlist}.reflector" | wc -l) -gt 0 ]] && mv "${mirrorlist}.reflector" "${mirrorlist}"
  fi
  msg2 "Updating the archlinux-keyring to avoid key signing issues during install"
  [ ! ${DRYRUN} = 'true' ] && pacman --quiet --noconfirm --disable-download-timeout -Sy archlinux-keyring
  return 0
}

_usesystemdresolved() {
  debug "_usesystemdresolved function"
  # systemd-resolved for resolver
  if [[ ${usesystemdresolved} == 'true' ]]; then
    msg2 "Creating symlink for systemd-resolved"
    [ ! ${DRYRUN} = 'true' ] && ln -sf /run/systemd/resolve/stub-resolv.conf ${_mountpath}/etc/resolv.conf || _err "Failed to create symlink for systemd-resolved"
    _enableservice systemd-resolved || _err "Failed to enable systemd-resolved service"
  fi
  return 0
}

_vgadd() {
  debug "_vgadd function"
  local _vgname="${1}"
  local _pvname="${2}"
  if [[ ! $(vgs --noheadings -oname | egrep "\ ${_vgname}$") ]]; then
    msg2 "Creating Volume Group ${_vgname} with block device ${_pvname}"
    [ ! ${DRYRUN} = 'true' ] && vgcreate -q -y -f "${_vgname}" "${_pvname}" >/dev/null || _err "Failed to create Volume Group ${_vgname} using Physical Volume ${_pvname}"
  else
    msg2 "Extending Volume Group ${_vgname} with block device ${_pvname}"
    [ ! ${DRYRUN} = 'true' ] && vgextend -q -y -f "${_vgname}" "${_pvname}" >/dev/null || _err "Failed to extend Volume Group ${_vgname} onto Physical Volume ${_pvname}"
  fi
  return 0
}

_virtualboxenv() {
  debug "_virtualboxenv function"
  # Test for running under Oracle's VirtualBox
  [[ $(systemd-detect-virt -v) == 'oracle' ]]
}

_vmwareenv() {
  debug "_vmwareenv function"
  # Test for running under VMware
  [[ $(systemd-detect-virt -v) == 'vmware' ]]
}

# Clears LVM Volume Groups, Physical Volume signatures, and Partition tables
# from disks in the 'diskdevs' array
_wipealldisks() {
  debug "_wipealldisks function"
  local _lvmvgname
  local _pvdisk
  local _diskdev
  msg "Wiping the disks"
  _cleanup || _err 'Function _cleanup returned an error'

  # Delete every LVM Volume Group
  for _lvmvgname in $( vgs --noheadings -oname ); do
    msg2 "Removing Volume Group ${_lvmvgname}"
    [ ! ${DRYRUN} = 'true' ] && vgremove -y "${_lvmvgname}" >/dev/null || _err "Failed to remove ${_lvmvgname} Volume Group"
  done

  # Wipe LVM PV signature off of every PV
  for _pvdisk in $( pvs --noheadings -oname ); do
    msg2 "Wiping LVM PV signature from ${_pvdisk}"
    [ ! ${DRYRUN} = 'true' ] && pvremove "${_pvdisk}" >/dev/null || _err "Failed to remove ${_pvdisk} Physical Volume"
  done

  # Wipe filesystems and partition tables off of all disks if autodisklist = 'true'
  # or just the disks listed in diskdevs array
  for _diskdev in "${diskdevs[@]}"; do
    msg2 "Wiping filesystem from ${_diskdev}"
    [ ! ${DRYRUN} = 'true' ] && wipefs -a "${_diskdev}" >/dev/null || _err "Failed to wipefs ${_diskdev} block device"
    msg2 "Wiping partition table from ${_diskdev}"
    [ ! ${DRYRUN} = 'true' ] && sgdisk -Z "${_diskdev}" >/dev/null || _err "Failed to Zap ${_diskdev} block device"
  done
  sync
  ignore_error partprobe
  return 0
}

debug "Finished function parsing"
### End of function definitions ###

msg "Setting _cleanup function as trap on exit"
debug "Do not set trap when debugging"
[ ! ${DEBUG} = 'true' ] && trap _cleanup 0
[ ${DEBUG} = 'true' ] && msg "Test message"
[ ${DEBUG} = 'true' ] && msg2 "Test message2"
[ ${DEBUG} = 'true' ] && warning "Test warning"
[ ${DEBUG} = 'true' ] && error "Test error"

# _usage
_archinstall && _completionbanner

# The _cleanup trap will fire here and do the work of sync'ing and unmounting

# TODO List:
# Add CLI parameterization and usage help
# Add LUKSs support
